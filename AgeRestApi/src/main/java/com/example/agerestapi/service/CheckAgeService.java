package com.example.agerestapi.service;

import com.example.agerestapi.model.Dad;
import com.example.agerestapi.model.Mom;
import com.example.agerestapi.model.UBND;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public class CheckAgeService {
    public static int count = 0;

    public static void check() {
        Dad dad = new Dad();
        Mom mom = new Mom();
        UBND ubnd = new UBND();

        ExecutorService executor = Executors.newFixedThreadPool(3);

        executor.execute(dad);
        executor.execute(mom);
        executor.execute(ubnd);

        executor.shutdown();

        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("có " + count + " kết quả trùng");
        boolean checkAge = false;
        if (count >= 2) {
            checkAge = true;
        }

        write(checkAge);
    }

    public static void write(boolean check) {
        try {
            FileWriter fw = new FileWriter("result.txt");
            fw.write(String.valueOf(check));
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("In kết quả thành công !");
    }
}
