package com.example.agerestapi.controller;

import com.example.agerestapi.service.CheckAgeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/age")
public class AgeController {

    private final Logger logger = LoggerFactory.getLogger(AgeController.class);


    public AgeController(CheckAgeService checkAgeService) {
    }

    @GetMapping("/check")
    boolean getAge(){
        logger.info("get logger");
        CheckAgeService.check();
        return true;
    }
}
