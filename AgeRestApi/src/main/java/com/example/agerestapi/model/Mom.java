package com.example.agerestapi.model;

import com.example.agerestapi.service.CheckAgeService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Mom extends Thread{

    @Override
    public void run() {
        try {
            if (checkAge(readFile()))
                CheckAgeService.count++;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int readFile() throws IOException {
        int age = 0;
        FileReader frd;
        BufferedReader bufR = null;

        try {
            frd = new FileReader("mom.txt");
            bufR = new BufferedReader(frd);
            String line ;
            while ((line = bufR.readLine()) != null)
            {
                age = 2021 - (Integer.parseInt(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert bufR != null;
        bufR.close();
        return age;

    }

    private boolean checkAge(int age) {
        return age == 21;
    }
}
