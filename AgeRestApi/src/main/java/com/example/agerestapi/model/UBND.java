package com.example.agerestapi.model;

import com.example.agerestapi.service.CheckAgeService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UBND extends Thread {

    @Override
    public void run() {
        try {
            if (checkAge(readFile()))
                CheckAgeService.count++;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Date readFile() throws IOException {
        Date birthday = new Date();
        FileReader frd;
        BufferedReader bufR = null;

        try {
            frd = new FileReader("ubnd.txt");
            bufR = new BufferedReader(frd);
            String line;
            while ((line = bufR.readLine()) != null) {
                try {
                    birthday = new SimpleDateFormat("dd/MM/yyyy").parse(line);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

        }
        assert bufR != null;
        bufR.close();

        return birthday;

    }

    private boolean checkAge(Date birthday) {
        int age = 0;
        Calendar born = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        if (birthday != null) {
            now.setTime(new Date());
            born.setTime(birthday);
            if (born.after(now)) {
                throw new IllegalArgumentException("Can't be born in the future");
            }
            age = now.get(Calendar.YEAR) - born.get(Calendar.YEAR);
            if (now.get(Calendar.DAY_OF_YEAR) < born.get(Calendar.DAY_OF_YEAR)) {
                age -= 1;
            }
        }
        return age == 21;
    }
}