package com.example.agerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgeRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgeRestApiApplication.class, args);
    }

}
